package com.citi.support.virtualization.supportvirtualization.supportvirtualization;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SupportvirtualizationApplicationTests {

	@Test
	public void contextLoads() {
	}

}
