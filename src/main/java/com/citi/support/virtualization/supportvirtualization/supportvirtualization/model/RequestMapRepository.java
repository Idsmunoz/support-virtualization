package com.citi.support.virtualization.supportvirtualization.supportvirtualization.model;

/*
* Copyright (C) 2016 by Citigroup. All rights reserved.
* Citigroup claims copyright in this computer program as an unpublished work,
* one or more versions of which were first used to provide services to
* customers on the dates indicated in the foregoing notice. Claim of
* copyright does not imply waiver of other rights.
*
* NOTICE OF PROPRIETARY RIGHTS
*
* This program is a confidential trade secret and the property of Citigroup.
* Use, examination, reproduction, disassembly, decompiling, transfer and/or
* disclosure to others of all or any part of this software program are
* strictly prohibited except by express written agreement with Citigroup.
*/

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * <code>RequestMapRepository</code>.
 *
 * @author salvador
 * @version 1.0
 */
 @Transactional
 @Repository
public interface RequestMapRepository extends CrudRepository<RequestMap, String>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.repository.CrudRepository#findAll()
	 */
	//public List<RequestMap> findAll();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.repository.CrudRepository#deleteById(java.lang.
	 * Object)
	 */
	//public void deleteById(String id);

	/**
	 * Find by request value.
	 *
	 * @return list
	 */
	//public List<RequestMap> findByRequest(String request);
	
}