package com.citi.support.virtualization.supportvirtualization.supportvirtualization.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.citi.support.virtualization.supportvirtualization.supportvirtualization.dto.RequestMapDTO;
import com.citi.support.virtualization.supportvirtualization.supportvirtualization.dto.ServiceResponse;
import com.citi.support.virtualization.supportvirtualization.supportvirtualization.model.RequestMap;
import com.citi.support.virtualization.supportvirtualization.supportvirtualization.model.RequestMapRepository;

@Service
public class VirtualClientServiceImpl implements VirtualClientService{

	@Autowired
	private RequestMapRepository requestMapRepository;

	@Value("${support.virtual.resourse}")
	private String resource;
	@Value("${support.virtual.resourse.all}")
	private String resourceAll;
	@Value("${support.virtual.resourse.add}")
	private String resourceAdd;
	@Value("${support.virtual.resourse.findOne}")
	private String resourceFindOne;
	@Value("${support.virtual.resourse.remove}")
	private String resourceDelete;
	
	
	
	@Override
	@Transactional(readOnly=true)
	public List<RequestMapDTO> findAll() {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		String path = resource.concat(resourceAll);
		ServiceResponse response = restTemplate.getForObject(path, ServiceResponse.class);
		List<RequestMapDTO>  list = (List<RequestMapDTO>) response.getResponse();
		
		return list;
		
	}

	@Override
	@Transactional
	public void deleteById(String id) {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		String path = resource.concat(resourceDelete).concat("/").concat(id);
		restTemplate.getForObject(path,ServiceResponse.class);
	}


	@Override
	@Transactional
	public void save(RequestMapDTO requestMap) {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		String path = resource.concat(resourceAdd);
		restTemplate.postForEntity(path, requestMap,RequestMapDTO.class);
	}

	@Override
	@Transactional(readOnly=true)
	public RequestMapDTO findOne(String id) {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		String path = resource.concat(resourceFindOne).concat("/").concat(id);
		ServiceResponse response = restTemplate.getForObject(path, ServiceResponse.class);
		List<RequestMapDTO>  list = (List<RequestMapDTO>) response.getResponse();
		
		return list.get(0);

	}



}
