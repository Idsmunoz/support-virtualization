package com.citi.support.virtualization.supportvirtualization.supportvirtualization.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.citi.support.virtualization.supportvirtualization.supportvirtualization.dto.RequestMapDTO;
import com.citi.support.virtualization.supportvirtualization.supportvirtualization.model.RequestMap;
import com.citi.support.virtualization.supportvirtualization.supportvirtualization.service.VirtualClientService;


@Controller
public class VirtualClientController {

	@Autowired
	private VirtualClientService virtualClientService;
	
	@RequestMapping(value="/listarRequest", method=RequestMethod.GET)
	public String getRequesMap(Model model) {
		model.addAttribute("titulo","List RequestMap");
		model.addAttribute("listReq", virtualClientService.findAll());
		return "listarRequest";
	}
	
	@RequestMapping(value="/addForm")
	public String addForm(Map<String, Object> model) {
		
		RequestMap rqm = new RequestMap();
		model.put("titulo", "Form RequestMap");
		model.put("requestMap", rqm);
		return "addForm";
		
	}
  
	@RequestMapping(value="/upForm/{id}")
	public String editar(@PathVariable(value="id") String id,Map<String,Object> model) {
		
 		RequestMapDTO rqm = null;
		if(!StringUtils.isEmpty(id)) {
			rqm = virtualClientService.findOne(id);
		}else {
			return "redirect:/listarRequest";
		}
		model.put("titulo", "Edit RequestMap: "+rqm.getId());
		model.put("requestMap", rqm);
		return "addForm";
	}
	
	@RequestMapping(value="/addForm", method=RequestMethod.POST)
	public String save(@Valid RequestMapDTO requestMapDTO,BindingResult result,RedirectAttributes flash,Model model ) {
	
		if(result.hasErrors()) {
			model.addAttribute("titulo", "Form RequestMap");
			return "addForm";			
		}
	
		virtualClientService.save(requestMapDTO);
		flash.addFlashAttribute("success", "Created with success!");
		return "redirect:/listarRequest";
	}
	
	@RequestMapping(value="/delete/{id}")
	public String delete(@PathVariable(value="id") String id,RedirectAttributes flash) {
		if(!StringUtils.isEmpty(id)) {
			virtualClientService.deleteById(id);
			flash.addFlashAttribute("error", "Removed!");
		}
		return "redirect:/listarRequest";
	}
}
