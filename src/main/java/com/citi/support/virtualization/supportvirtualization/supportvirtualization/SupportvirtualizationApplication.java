package com.citi.support.virtualization.supportvirtualization.supportvirtualization;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupportvirtualizationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupportvirtualizationApplication.class, args);
	}
}
