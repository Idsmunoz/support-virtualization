package com.citi.support.virtualization.supportvirtualization.supportvirtualization.service;

import java.util.List;

import com.citi.support.virtualization.supportvirtualization.supportvirtualization.dto.RequestMapDTO;
import com.citi.support.virtualization.supportvirtualization.supportvirtualization.model.RequestMap;

public interface VirtualClientService {
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.repository.CrudRepository#findAll()
	 */
	public List<RequestMapDTO> findAll();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.data.repository.CrudRepository#deleteById(java.lang.
	 * Object)
	 */
	public void deleteById(String id);


	public void save(RequestMapDTO requestMap);
	public RequestMapDTO findOne(String id);
}
